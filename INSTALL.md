# Installation

Syntement is a stand-alone Python 3 script.
It depends on the following Python modules:

* [Biopython](https://biopython.org)
* IntervalTree
* [PyInterval](https://pyinterval.readthedocs.io/en/latest/) (for computing synteny block genome coverage)
  Syntement can still run without it; you will just not get a coverage report.
* [Multiprocess](https://github.com/uqfoundation/multiprocess)
* networkx

Installation can be done as:

```
pip install .
```

You may need to add `$HOME/.local/bin` to your `$PATH`.

```
export PATH="$HOME/.local/bin:$PATH"
```
