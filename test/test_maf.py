import os
import shutil

import pytest

from syntement.io import maf


@pytest.mark.parametrize("input,expected", [
    # 2-0059, Cactus(SNP)-nooutgroup
    ({'srcSize':4425172,'size':84,'start':1741442,'strand':-1}, (2683647,2683730)),
    # 4-0010, Cactus(SNP)-nooutgroup
    ({'srcSize':4400007,'size':1000,'start':28693,'strand':1}, (28694,29693)),
])
def test_get_coords_maf(input, expected):
    assert maf.get_maf_coords(input) == expected
