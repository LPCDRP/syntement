import logging

import pytest

from syntement import discern
from syntement.datatypes import Block, Permutation
from syntement.external.phylogeny import Phylogeny


def PermFromList(perm_data):
    return [
        Permutation(
            genome, chrom,
            blocks=[Block(block_id=abs(block), sign=int(block/abs(block))) for block in perm]
        ) for genome, chrom, perm in perm_data
    ]

@pytest.mark.parametrize('case', [
    pytest.param('inversion', marks=pytest.mark.xfail(reason='known issue')),
    'hodgepodge',
    'jumbled_repeats',
    'all_duplicated',
])
def test_resolve_repeats(case):
    logging.basicConfig(level=logging.DEBUG)

    if case == 'inversion':
        permutations = PermFromList([
            ['A', '1', [1,  4, 2, 3, 4, 5]],
            ['B', '1', [1, -4, 2, 3, 4, 5]],
            ['C', '1', [1,  4, 2, 3, 5]],
        ])
        expected = PermFromList([
            ['A', '1', [1,  4.1, 2, 3, 4.2, 5]],
            ['B', '1', [1, -4.1, 2, 3, 4.2, 5]],
            ['C', '1', [1,  4.1, 2, 3,      5]],
        ])
    elif case == 'hodgepodge':
        permutations = PermFromList([
            ['A', '1', [1, 4, 2, 3, 4, 5, 6,     7, 8, 4, 9, 10, 4, 11, 12, 13, 4, 4, 4,    14]],
            ['B', '1', [1, 4, 2, 3, 4, 5, 6, -4, 7, 8,    9, 10, 4, 11, 12, 13, 4, 4, 4, 4, 14]],
            ['C', '1', [1, 4, 2, 3,    5, 6,     7, 8, 4, 9, 10, 4, 11, 12, 13, 4, 4, 4,    14]],
        ])
        expected = PermFromList([
            ['A', '1', [
                1, 4.1, 2, 3, 4.2, 5, 6,       7, 8, 4.3, 9, 10, 4.4, 11, 12, 13, 4.5, 4.6, 4.7,      14
            ]],
            ['B', '1', [
                1, 4.1, 2, 3, 4.2, 5, 6, -4.8, 7, 8,      9, 10, 4.4, 11, 12, 13, 4.5, 4.6, 4.7, 4.9, 14
            ]],
            ['C', '1', [
                1, 4.1, 2, 3,      5, 6,       7, 8, 4.3, 9, 10, 4.4, 11, 12, 13, 4.5, 4.6, 4.7,      14
            ]],
        ])
    elif case == 'jumbled_repeats':
        permutations = PermFromList([
            ['A', '1', [1, 4, 2, 3, 4, 5, 5, 6, 7, 8, 4, 9, 10, 4, 11, 12, 13, 4, 4, 5, 5, 14]],
            ['B', '1', [1, 4, 2, 3, 4, 5,    6, 7, 8,    9, 10, 4, 11, 12, 13, 4, 4, 4, 4, 14]],
            ['C', '1', [1, 4, 2, 3,       5, 6, 7, 8, 4, 9, 10, 4, 11, 12, 13, 4, 4, 4, 5, 14]],
        ])
        expected = PermFromList([
            ['A', '1', [
                1, 4.1, 2, 3, 4.2, 5.1, 5.2, 6, 7, 8, 4.3, 9, 10, 4.4, 11, 12, 13, 4.5, 4.6, 5.3, 5.4, 14
            ]],
            ['B', '1', [
                1, 4.1, 2, 3, 4.2, 5.1,      6, 7, 8,      9, 10, 4.4, 11, 12, 13, 4.5, 4.6, 4.7, 4.8, 14
            ]],
            ['C', '1', [
                1, 4.1, 2, 3,           5.2, 6, 7, 8, 4.3, 9, 10, 4.4, 11, 12, 13, 4.5, 4.6, 4.7, 5.4, 14
            ]],
        ])
    elif case == 'all_duplicated':
        permutations = PermFromList([
            ['A', '1', [1, 1, 2, 2, 4, 4]],
            ['B', '1', [2, 2, 1, 1, 4, 4]],
            ['C', '1', [1, 1, 4, 4, 2, 2]],
        ])
        expected = PermFromList([
            ['A', '1', [1.1, 1.2, 2.1, 2.2, 4.1, 4.2]],
            ['B', '1', [2.3, 2.4, 1.3, 1.4, 4.3, 4.4]],
            ['C', '1', [1.3, 1.4, 4.3, 4.4, 2.3, 2.4]],
        ])


    phylogeny = Phylogeny.from_newick("(B:2, (A, C):1)")

    repeats = discern.find_repeats(permutations)

    discern.resolve_repeats(
        perms=permutations,
        repeats=repeats,
        phylogeny=phylogeny,
    )
    assert [p.blocks for p in permutations] == [p.blocks for p in expected]
