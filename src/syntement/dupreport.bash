#!/bin/bash

usage () {
    >&2 echo 'Usage:'
    >&2 echo 'count-duplicates [-o|--output-list duplicates.txt] synteny-file.dat'
    >&2 echo
    >&2 echo "output columns are:"
    >&2 echo "avg_unique_fraction, n_duplicate_markers, n_duplicate_occurrences, avg_duplicity, max_duplicity, sample_with_max, max_dupe_block_id"
    >&2 echo "if -o/--output-list outfile is passed, outfile will contain all duplicated blocks."
    >&2 echo "Column headers in this file are: count, block id, sample"
}

summary() {
    awk -v OFS=$'\t' -f  <( cat <<'__COMMAND__'
function value() {
    return $5"\t"$4
} {
    totaluniqfrac+=$1
    occurrences+=$2;
    if(min=="") {min=max=$3; minval=maxval=value()};
    if($3>max) {max=$3; maxval=value()};
    if($3<min) {min=$3; minval=value()};
    count++
} END {
    print occurrences, totaluniqfrac/count, max, maxval;
}
__COMMAND__
			  )
}

ARGS=$(getopt -o 'o:h' --long output-list:help  -n 'count-duplicates' -- "$@")

if [ $? -ne 0 ]
then
    usage
    exit 1
fi

eval set -- "$ARGS"
unset ARGS

dupes_list=".dupes.txt"

while true
do
    case "$1" in
	-o|--output-list)
	    dupes_list=$(realpath "$2")
	    # this file will be appended to, so wipe it out if it exists already
	    rm -f "$dupes_list"
	    shift 2
	    ;;
	-h|--help)
	    usage
	    exit
	    ;;
	'--')
	    shift
	    break
	    ;;
	*)
	    usage
	    exit 2
	    ;;
    esac
done

if [ $? -ne 0 ]
then
    usage
    exit 3
fi

tmp=$(mktemp -d)

trap "mv ${tmp} ${tmp}.del && rm -rf ${tmp}.del" EXIT

if [ "$#" -eq 0 ]
then
    infile=$(mktemp -p $tmp);
    cat > "$infile";
else
    infile=$(realpath "$1")
fi

cd $tmp \
    && cat "$infile" \
	| sed -r '/^[^>]/s/[\+\-]//g' `# only remove +/- from non-header lines` \
	| split -l2 \
    && for f in *;
    do
	sample_id=$(head -n1 "$f" | sed 's/>//')
	unique=$(tail -n1 "$f" \
		     | tr ' ' '\n' \
		     | sort \
		     | uniq -u \
		     | wc -l)
	# track duplicate markers we see in this particular sample
	tail -n1 "$f" \
	    | tr ' ' '\n' \
	    | sort \
	    | uniq -cd \
	    | sed 's/^[[:space:]]*//' \
	    | awk -v OFS='	' -v id="$sample_id" '{print $1,$2,id'} >> "$dupes_list"
	total=$(tail -n1 "$f" \
	    | tr ' ' '\n' \
	    | sort \
	    | wc -l)
	max_dupe=$(cut -f1,2 "$dupes_list" \
		       | sort -nk1 \
		       | tail -n1 \
		)
	n_dupe_occurrences=$(tail -n1 "$f" \
				 |  tr ' ' '\n' \
				 | sort \
				 | uniq -cd \
				 | awk '{sum += $1} END {print sum}')
	test -n "$max_dupe" || max_dupe="0 -"
	test -n "$n_dupe_occurrences" || n_dupe_occurrences=0
	echo              "$unique $total  $n_dupe_occurrences $max_dupe $sample_id" \
	    | awk '{print($1/$2,           $3,                 $4,$5,    $6)}'
    done | summary > .summary.txt

paste <(cut -f2 "$dupes_list" | sort | uniq | wc -l)  `# n duplicate markers` \
      <(awk '{n+=$1;i++} END {if (i != 0) print n/i; else print 0}' "$dupes_list") `# avg duplicity` \
      .summary.txt \
    | awk -v OFS='\t' '{out=sprintf("%.3f %d %d %.3f %d %s %s", $4, $1, $3, $2, $5, $6, $7); gsub(/ /,OFS,out);print out}'
# 1: n_duplicate_markers
# 2: avg_duplicity
# 3: n_duplicate_occurrences
# 4: avg_unique_fraction
# 5: max_duplicity
# 6: sample_with_max
# 7: max_dupe_block_id
