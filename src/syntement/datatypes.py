#(c) 2013-2014 by Ragout Authors
#(c) 2024 Afif Elghraoui
# This file is derived from the Ragout program.

class Block:
    """
    Represents synteny block
    """
    def __init__(self, block_id, sign, start=None, end=None):
        self.block_id = str(block_id)
        self.sign = sign
        self.start = int(start) if start is not None else None
        self.end = int(end) if end is not None else None

    def length(self):
        if self.start is None or self.end is None:
            return None

        assert self.end >= self.start
        return self.end - self.start + 1

    def signed_id(self):
        """
        ragout's original function multiplies the two quantities.
        When the block_id is a string, multiplying it by -1 erases it.
        """
        return f"{'-' if self.sign == -1 else ''}{self.block_id}"

    def __repr__(self):
        return self.signed_id()

    def __eq__(self, other):
        return (
            self.block_id == other.block_id
            and self.sign == other.sign
            and self.start == other.start
            and self.end == other.end
        )


class Permutation:
    """
    Represents signed permutation
    """
    def __init__(self, genome_name, chr_name, seq_len=None, blocks=None):
        self.genome_name = genome_name
        self.chr_name = chr_name
        self.seq_start = 0
        self.seq_end = seq_len
        self.seq_len = seq_len
        if blocks is None:
            self.blocks = []
        else:
            self.blocks = blocks
        self.repeat_id = 0
        self.draft = False

    def length(self):
        if self.start is None or self.end is None:
            return None

        assert self.seq_end > self.seq_start
        return self.seq_end - self.seq_start

    def name(self):
        return '.'.join([self.genome_name, self.chr_name])

    def iter_pairs(self):
        for pb, nb in zip(self.blocks[:-1], self.blocks[1:]):
            yield pb, nb

    def __lt__(self, other):
        return repr(self) < repr(other)

    def __repr__(self):
        return ("[{0}, {1}, {2}, b:{3}, e:{4}]"
                    .format(self.genome_name, self.chr_name,
                            [b.signed_id() for b in self.blocks],
                            self.seq_start, self.seq_end))

    #
    # TODO: This currently causes issues in repeat_resolver due to making Permutation unhashable.
    # Permutation should be unhashable, but current implementation relies on putting them in sets
    # for the cases of repetitive matches.
    #
    #def __eq__(self, other):
    #    return (
    #        self.genome_name == other.genome_name
    #        and self.chr_name == other.chr_name
    #        and self.seq_start == other.seq_start
    #        and self.seq_end == other.seq_end
    #        and self.seq_len == other.seq_len
    #        and self.blocks == other.blocks
    #    )
