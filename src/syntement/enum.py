from collections import defaultdict
import fileinput
import sys

from .io.permutations import parse_auto


def main(permutations_file_list, outfile=None):

    permutations = parse_auto(
        fileinput.input(files=permutations_file_list),
    )[0]

    enumerator = enum(permutations)
    
    if not outfile:
        outfile = sys.stdout

    for name, number in enumerator.items():
        print(
            '\t'.join([str(name), str(number)]),
            file=outfile
        )


def enum(permutations_list):

    global next_id
    next_id = 0
    def assign_number():
        global next_id
        next_id += 1
        return next_id

    enumerator = defaultdict(assign_number)

    for perm in permutations_list:
        for block in perm.blocks:
            enumerator[block.block_id]

    return enumerator
