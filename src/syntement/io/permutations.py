import itertools

from Bio import AlignIO

from ..datatypes import Permutation, Block
from .maf import get_maf_coords


class PermException(Exception):
    pass

def tokenize_seq(seq_id):
    tokens = seq_id.split('.', maxsplit=1)
    if len(tokens) != 2:
        raise PermException((
            "sequence ids "
            "do not follow naming convention: "
            "'genome.chromosome'"
        ))
    genome_name, chr_name = tokens
    return genome_name, chr_name

def parse_auto(fh):
    firstline = next(fh).strip()
    if firstline.startswith('>'):
        permutations = parse_unimog(itertools.chain([firstline], fh))
        writer=write_unimog
    else:
        permutations = parse_bed(itertools.chain([firstline], fh))
        writer=write_bed
    return permutations, writer

def parse_bed(fh):
    """
    """
    perms = {}
    for line in fh:
        if line.startswith('#'):
            continue
        seq_id, start, end, name, score, strand = line.strip().split('\t')[0:6]
        if seq_id not in perms:
            genome_name, chr_name = tokenize_seq(seq_id)
            perms[seq_id] = Permutation(
                genome_name,
                chr_name,
            )
        perms[seq_id].blocks.append(Block(
            block_id=name,
            sign=(1 if strand == '+' else -1),
            start=int(start) + 1,
            end=int(end),
        ))
    for seq in perms:
        perms[seq].blocks = sorted(perms[seq].blocks, key=lambda b: (b.start, b.end))

    return perms.values()

def write_bed(permutations, ofh):
    for perm in permutations:
        for block in perm.blocks:
            ofh.write("\t".join([
                perm.name(),
                str(block.start - 1),
                str(block.end),
                block.block_id,
                "1", # dummy entry for "score" field
                '+' if block.sign == 1 else '-',
            ])+ "\n")


def parse_gff(fh, feature_types=None, id_tag='ID', *args, **kwargs):
    permutations = {}
    id_tag = id_tag.upper()

    if feature_types:
        feature_types  = [t.upper() for t in feature_types]

    for line in fh:
        line = line.strip()
        if line == '##FASTA':
            break
        if line.startswith('##sequence-region'):
            [_, sample_id, _, sequence_length] = line.split(' ')
            genome_name, chr_name = tokenize_seq(sample_id)
            permutations[sample_id] = Permutation(
                genome_name,
                chr_name,
                seq_len=int(sequence_length),
            )
            continue
        elif line.startswith('#'):
            continue

        [
            sample_id,
            source,
            type,
            start,
            stop,
            score,
            strand,
            phase,
            attrib_str,
        ] = line.split('\t')
        if feature_types and type.upper() not in feature_types:
            continue
        attributes = {k.upper():v for k,v in [keyval.split('=') for keyval in attrib_str.split(';')]}
        if not id_tag in attributes:
            continue
        name = attributes[id_tag]
        start = int(start)
        stop = int(stop)
        sign = -1 if strand=='-' else 1

        # sequence was not described in header
        if not sample_id in permutations:
            genome_name, chr_name = tokenize_seq(sample_id)
            permutations[sample_id] = Permutation(genome_name, chr_name)

        permutations[sample_id].blocks.append(
            Block(name, sign, start, stop)
        )

    permutations = list(permutations.values())
    for perm in permutations:
        perm.blocks = sorted(perm.blocks, key=lambda b: (b.start, b.end))

    return permutations

def parse_maf(fh, min_block_len=0, *args, **kwargs):
    permutations = {}
    block_id = 0

    for segment in AlignIO.parse(fh, "maf"):
        block_id += 1
        if len(segment[0]) < min_block_len:
            continue
        for sample in segment:
            if not sample.id in permutations:
                genome_name, chr_name = tokenize_seq(sample_id)
                permutations[sample.id] = Permutation(
                    genome_name,
                    chr_name,
                    seq_len=sample.annotations['srcSize']
                )

            # https://biopython.org/wiki/Multiple_Alignment_Format
            (start, stop) = get_maf_coords(sample.annotations)

            permutations[sample.id].blocks.append(
                Block(block_id, sample.annotations['strand'], start, stop)
            )

    permutations = list(permutations.values())
    for perm in permutations:
        perm.blocks = sorted(perm.blocks, key=lambda b: (b.start, b.end))

    return permutations

def parse_unimog(fh):
    """
    Parse a UniMog-style permutations file into a Ragout Permutation object.
    Uses some code/style from Ragout's permutation._parse_blocks_coords().
    """
    perms = []
    for line in fh:
        if line.startswith(">"):
            genome_name, chr_name = tokenize_seq(line[1:].strip())
            perms.append(Permutation(
                genome_name,
                chr_name,
            ))
        else:
            for element in line.split():
                if element[0] in ['+', '-']:
                    sign = 1 if element[0] == '+' else -1
                    block_id = element[1:]
                # linear/circular chromosome ends
                # (not used by ragout's object but might be encountered)
                elif element in ['|', ')', '$']:
                    break
                else:
                    sign = 1
                    block_id = element
                perms[-1].blocks.append(
                    Block(block_id, sign)
                )
    return perms

def write_unimog(permutations, ofh):
    """
    (c) 2013-2014 by Ragout Authors
    aka output_permutations()
    """
    for perm in permutations:
        ofh.write(">" + perm.name() + "\n")
        for block in perm.blocks:
            ofh.write("{0} ".format(block.signed_id()))
        ofh.write("$\n")
