def get_maf_coords(maf_annotations):
    start = maf_annotations['start']
    stop  = maf_annotations['start'] + maf_annotations['size']
    if maf_annotations['strand'] == -1:
        start = maf_annotations['srcSize'] - stop
        stop = maf_annotations['srcSize'] - maf_annotations['start']
    return (start+1, stop)
