import os
from collections import defaultdict

from intervaltree import IntervalTree, Interval


bed_ext = [
    '.bed',
]

gff_ext = [
    '.gff',
    '.gff3',
]

def main(ref_coords_file, qry_coords_file, report):
    qry_ext = os.path.splitext(qry_coords_file)[1]
    if qry_ext in gff_ext:
        qry_loader = load_gff_intervals
    else:
        qry_loader = load_bed_intervals
    qry_blocks, qry_intervals, qry_block_intervals = qry_loader(
        qry_coords_file
    )

    ref_ext = os.path.splitext(ref_coords_file)[1]
    if ref_ext in gff_ext:
        ref_loader = load_gff_intervals
    else:
        ref_loader = load_bed_intervals
    ref_blocks, ref_intervals, ref_block_intervals = ref_loader(
        ref_coords_file
    )

    mapping, mapping_by_chrom = match(
        blocks=ref_blocks,
        block_intervals=ref_block_intervals,
        qry_intervals=qry_intervals,
    )

    if report:
        with open(report, 'r') as rf:
            header = next(rf).strip()
            header += f"\tblock_ann\tneighbor_ann"
            print(header)
            for line in rf:
                line = line.strip()
                if not line.startswith('//'):
                    extrem = line.split('\t')[0]
                    # remove the _h and _t suffix
                    block = extrem[:-2]
                else:
                    chrom, neighbor = line.split('\t')[-2:]
                    if chrom not in mapping_by_chrom[block]:
                        chrom = f"{chrom}.1"
                    if neighbor == 'DELETED':
                        block_map = '..'
                        neigh_map = '..'
                    else:
                        block_map = mapping_by_chrom[block][chrom]
                        neigh_map = mapping_by_chrom[neighbor[:-2]][chrom]
                    line += f"\t{block_map}\t{neigh_map}"
                print(line)
        return

    for block in mapping:
        print("\t".join([
            block,
            ";".join(mapping[block])
        ]))

    return

def load_bed_intervals(coords_file):
    blocks = set()
    intervals_by_seq = defaultdict(IntervalTree)
    intervals_by_block_by_seq = defaultdict(lambda: defaultdict(set))

    with open(coords_file, 'r') as cf:
        for line in cf.readlines():
            if line.startswith('#'):
                continue
            chrom, start, end, name, score, strand = line.strip().split('\t')[0:6]
            # BED is half-open (start, end] and IntervalTree is half-open the other way [start, end)
            start = int(start) + 1
            end = int(end) + 1
            interval = Interval(start, end, name)

            blocks.add(name)
            intervals_by_seq[chrom].add(interval)
            intervals_by_block_by_seq[chrom][name].add(interval)

    return blocks, intervals_by_seq, intervals_by_block_by_seq

def load_gff_intervals(coords_file):
    blocks = set()
    intervals_by_seq = defaultdict(IntervalTree)
    intervals_by_block_by_seq = defaultdict(lambda: defaultdict(set))

    with open(coords_file, 'r') as cf:
        for line in cf.readlines():
            if line.startswith('#'):
                continue
            (
                chrom,
                _,
                feature_type,
                start,
                end,
                _,
                strand,
                _,
                attribs
            ) = line.strip().split('\t')
            if feature_type in ['databank_entry', 'gene']:
                continue
            attribs = {
                key.lower(): val for key, val in
                [datum.split('=') for datum in attribs.split(';')]
            }
            if 'locus_tag' not in attribs:
                continue
            if 'gene' in attribs:
                name = attribs['gene']
            elif 'product' in attribs:
                name = attribs['product'].replace(" ","_")
            else:
                name = attribs['locus_tag']

            start = int(start)
            end = int(end) + 1
            interval = Interval(start, end, name)

            blocks.add(name)
            intervals_by_seq[chrom].add(interval)
            intervals_by_block_by_seq[chrom][name].add(interval)

    return blocks, intervals_by_seq, intervals_by_block_by_seq

def match(blocks, block_intervals, qry_intervals):
    """
    """
    mapping = defaultdict(set)
    mapping_by_chrom = defaultdict(dict)

    for block in blocks:
        for chrom in block_intervals:
            for block_instance in block_intervals[chrom][block]:
                qry_overlappers = sorted(
                    qry_intervals[chrom].overlap(block_instance),
                    key=lambda iv: (iv.begin, iv.end),
                )
                chrom_map = []
                block_points = set(range(block_instance.begin, block_instance.end))
                for qry in qry_overlappers:
                    qry_len = qry.end - qry.begin
                    qry_points = set(range(qry.begin, qry.end))
                    ovl_len = len(block_points & qry_points)
                    ovl_frac = ovl_len / qry_len
                    if ovl_frac < 1:
                        slug = f"{qry.data}({ovl_frac:.2})"
                    else:
                        slug = qry.data
                    chrom_map.append(slug)
                    mapping[block].add(qry.data)
                mapping_by_chrom[block][chrom] = ';'.join(chrom_map)

    return mapping, mapping_by_chrom
