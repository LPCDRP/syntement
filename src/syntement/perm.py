from .io import permutations


def main(coords_file, outfile):
    perm_list = permutations.parse_bed(coords_file)

    permutations.write_unimog(
        perm_list,
        outfile,
    )
