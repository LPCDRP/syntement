from argparse import ArgumentParser
import os
import sys
from warnings import warn

try:
    from interval import interval
except ImportError:
    pass

from .io.permutations import (
    parse_gff,
    parse_maf,
    write_bed,
    write_unimog,
)


def main(
        infiles,
        outdir,
        feature_types,
        id_tag,
        min_block_len,
):

    if not os.path.isdir(outdir):
        try:
            os.mkdir(outdir)
        except OSError:
            print("Could not create output directory: " + outdir,
                  file=sys.stderr)
            exit(1)

    compute_coverage = 'interval' in sys.modules

    if compute_coverage:
        coverage_report = os.path.join(outdir, 'coverage_report.txt')
        warned = False
    else:
        coverage_report = os.devnull
        print(
            "Warning: PyInterval is not installed. Coverage reports will not be produced.",
            file=sys.stderr,
        )
        warned = True
    blockfile = os.path.join(outdir, 'genomes_permutations.txt')
    coords_file = os.path.join(outdir, 'blocks_coords.bed')
    genomes_file = os.path.join(outdir, 'seqs.genome')
    sign = {1:'+',
            '+':'+',
            -1:'-',
            '-':'-'}

    exts = set([os.path.splitext(f)[1] for f in infiles])
    if len(exts) > 1:
        sys.exit("Multiple file types given. "
                 "Please use either .maf or .gff files--not both.")
    ext = exts.pop()
    if ext == '.maf' and len(infiles) > 1:
        sys.exit("Multiple input files only supported for gff. "
                 "MAF files must be combined or processed individually.")
    elif ext == '.gff' and min_block_len:
        print("Warning: --minBlockLen is currently only supported for MAF inputs. ignoring...",
              file=sys.stderr)

    process = {
        '.maf':parse_maf,
        '.gff':parse_gff,
    }

    # empty feature_types corresponds to 'all' in parse_gff
    if 'all' in feature_types:
        feature_types = None

    permutations = []
    for infile in infiles:
        with open(infile, 'r') as source:
            permutations.extend(process[ext](
                fh=source,
                feature_types=feature_types,
                id_tag=id_tag,
                min_block_len=min_block_len,
            ))

    with open(coords_file, 'w') as cf:
        write_bed(permutations, cf)

    with open(blockfile, 'w') as bf:
        write_unimog(permutations, bf)

    with open(coverage_report,'w') as cr:
        for perm in permutations:
            regions = interval(*((b.start, b.end) for b in perm.blocks))
            sample = perm.name()
            if perm.seq_len is None:
                perm.seq_len = int(regions.extrema[-1][-1])
                if not warned:
                    warn("Genome size information was not available for " +
                         sample + ". Using the maximum interval boundary as genome length for determining coverage...")
                    warned = True
            if compute_coverage:
                coverage = sum([abs(a-b) for a,b in regions])/perm.seq_len
                print('\t'.join([
                    perm.name(),
                    str(coverage*100),
                    str(perm.seq_len),
                ]), file=cr)

    with open(genomes_file, 'w') as gf:
        for perm in permutations:
            print(
                '\t'.join([perm.name(), str(perm.seq_len)]),
                file=gf,
            )
