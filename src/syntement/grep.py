import fnmatch

from .io.permutations import parse_unimog


def main(
        pattern,
        permutations_file,
        seqs,
        context=5,
):
    permutations = parse_unimog(permutations_file)
    results = {}

    for perm in permutations:
        seq_id = f"{perm.genome_name}.{perm.chr_name}"
        if seqs is not None:
            # The previous iteration had our last target sequence
            if not seqs:
                break
            target = ''
            if perm.genome_name in seqs:
                target = perm.genome_name
            elif seq_id in seqs:
                target = seq_id
            if not target:
                continue
            else:
                seqs.remove(target)

        matches = grep(perm.blocks, pattern, context)
        if matches:
            results[seq_id] = matches

    for seq_id in results:
        print(f"> {seq_id}")
        for hit in results[seq_id]:
            hit = [str(h) for h in hit]
            print("\t".join(hit))

def grep(block_list, pattern, context=5):
    # fnmatch.filter is supposed to be faster,
    # but it uses case-insensitive matching (fnmatch rather than fnmatchcase)
    # and doesn't help us identify context.

    matches = []
    for i in range(len(block_list)):
        block = block_list[i]
        if fnmatch.fnmatchcase(block.block_id, pattern):
            matches.append(i)

    if context == 0:
        result = [
            [block_list[i]] for i in matches
        ]
    else:
        result = []
        for hit in matches:
            context_start = max(0, hit - context)
            context_end = min(len(block_list), hit + context + 1)
            result.append(block_list[context_start:context_end])

    return result
