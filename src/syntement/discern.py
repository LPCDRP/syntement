from collections import defaultdict
from copy import deepcopy, copy
from io import StringIO
from itertools import chain, product, combinations
import sys
import logging
from multiprocess import Pool

from Bio import Phylo
import networkx as nx

from .external.phylogeny import Phylogeny
from .io.permutations import (
    parse_bed,
    write_bed,
    parse_unimog,
    write_unimog,
)


logger = logging.getLogger(__name__)

def main(permutations_file, tree_file, nproc, outfile):

    logging.basicConfig(level=logging.DEBUG)

    if permutations_file.name.endswith('.bed'):
        parse = parse_bed
        write = write_bed
    else:
        parse = parse_unimog
        write = write_unimog

    permutations = parse(permutations_file)

    # ragout has some specialized tree parsing/manipulation,
    # but it can't handle sample names that begin with numbers.
    # Until we can switch over to standardized methods, we need to do this dance:
    # - use Biopython to read the tree
    # - add a text prefix "SYN" to all leaf node names
    # - write the tree to a temporary in-memory file
    # - parse the modified tree using ragout
    # - remove the SYN prefixes
    tree = Phylo.read(tree_file, "newick")
    for leaf in tree.get_terminals():
        leaf.name = f"SYN{leaf.name}"
    tree_memfile = StringIO()
    Phylo.write(tree, tree_memfile, "newick")
    tree_memfile.seek(0)
    # ragout doesn't like biopython's phantom branch
    newick_str = tree_memfile.read().replace(":0.00000;",";").strip()
    phylogeny = Phylogeny.from_newick(newick_str)
    for leaf in phylogeny.tree.leaves:
        leaf.identifier = leaf.identifier[3:]


    #
    # Prevent a late index error in _split_into_profiles
    #
    tree_leaves = [leaf.identifier for leaf in phylogeny.tree.leaves]
    for perm in permutations:
        if perm.genome_name not in tree_leaves:
            sys.exit(f"Tree file {tree_file} does not contain genome {perm.genome_name}.")

    repeats = find_repeats(permutations)

    resolve_repeats(
        permutations,
        repeats,
        phylogeny,
        nproc,
    )

    write(
        permutations=permutations,
        ofh=outfile,
    )

def find_repeats(permutations):
    """
    Returns a set of repetitive blocks
    (c) 2013-2014 by Ragout Authors
    Released under the BSD license
    """
    index = defaultdict(set)
    repeats = set()
    for perm in permutations:
        for block in perm.blocks:
            if perm.genome_name in index[block.block_id]:
                repeats.add(block.block_id)
            else:
                index[block.block_id].add(perm.genome_name)
    return repeats

# The below is
# (c) 2013-2015 by Ragout Authors (BSD Licensed)
# (c) 2023-2024 Afif Elghraoui
#
# derived from ragout's repeat_resolver.py module

class Context:
    """
    (c) 2013-2015 by Ragout Authors
    Released under the BSD license
    """
    def __init__(self, perm, pos, left, right):
        self.perm = perm
        self.pos = pos
        self.left = left
        self.right = right

    def __str__(self):
        return "({0}, {1}, {2}, {3})".format(self.perm.chr_name, self.pos,
                                             self.left, self.right)

    def equal(self, other):
        return self.right == other.right and self.left == other.left

    def __eq__(self, other):
        return (
            self.perm.genome_name == other.perm.genome_name
            and self.pos == other.pos
            and self.left == other.left
            and self.right == other.right
        )

    def __hash__(self):
        return hash((self.perm.genome_name, self.perm.chr_name, self.pos))


def resolve_repeats(
        perms,
        repeats,
        phylogeny,
        nproc=1,
):
    logger.info("Resolving repeats")
    logger.debug("Unique repeat blocks: %d", len(repeats))

    # multiprocessing breaks context.perm's reference, so we need to be able to find the originals later
    perm_dict = defaultdict(dict)
    n_resolved = 0
    for perm in perms:
        perm_dict[perm.genome_name][perm.chr_name] = perm

    contexts = _get_contexts(perms, repeats)

    #getting matches
    repetitive_matches = []
    unique_matches = []
    def build_profiles(repeat_id, contexts=contexts, repeats=repeats, phylogeny=phylogeny):
        by_genome = defaultdict(list)
        for ctx in contexts[repeat_id]:
            by_genome[ctx.perm.genome_name].append(ctx)

        unique_m = _split_into_profiles(by_genome, repeats, phylogeny)
        return unique_m

    with Pool(nproc) as p:
        match_profiles = p.map(build_profiles, sorted(contexts))

    unique_matches = list(chain.from_iterable(match_profiles))
    ##

    matched_contigs = set()
    for m in repetitive_matches:
        if all([(b.block_id in repeats) for b in m.trg.perm.blocks]):
            matched_contigs.add(m.trg.perm)
    logger.debug("Repetitive sequences with matches: %d", len(matched_contigs))

    logger.debug("Unique matches: %d", len(unique_matches))
    logger.debug("Repetitive matches: %d", len(repetitive_matches))

    instance_counter = defaultdict(lambda:0)

    # https://github.com/fenderglass/Ragout/commit/e524a697defe5dbd05e9b8962fad2696869ad1eb
    ##resolving unique
    for ctxes in unique_matches:
        ids = [ctx.perm.blocks[ctx.pos].block_id for ctx in ctxes]
        assert ids.count(ids[0]) == len(ids) # make sure all names identical
        base_id = ids[0]
        instance_counter[base_id] += 1
        for ctx in ctxes:
            perm_dict[ctx.perm.genome_name][ctx.perm.chr_name].blocks[ctx.pos].block_id = f"{base_id}.{instance_counter[base_id]}"
        n_resolved += 1
    ##

    logger.debug("Resolved %d unique repeat instances", n_resolved)


def _split_into_profiles(contexts_by_genome, repeats, phylogeny):
    """
    Given repeat contexts in each of reference genomes,
    joins them into "profiles" -- sets of matched contexts
    across different genomes (like an alignemnt column in MSA)
    """

    def is_unique(context):
        return any(b not in repeats for b in
                   [b.block_id for b in context.perm.blocks])

    def name_node(context):
        return f"{context.perm.genome_name}.{context.perm.chr_name}_{context.pos}"

    references = set(contexts_by_genome.keys())
    genomes = [g for g in phylogeny.terminals_dfs_order() if g in sorted(references)]
    profiles  = [[c] for c in contexts_by_genome[genomes[0]]]

    unique_matches = []
    repetitive_matches = []

    for genome in genomes[1:]:
        #finding a matching between existing profiles and a new genome
        genome_ctxs = contexts_by_genome[genome]

        t_unique = [c for c in genome_ctxs if is_unique(c)]
        t_repetitive = [c for c in genome_ctxs if not is_unique(c)]
        graph = nx.Graph()

        profile_updates = defaultdict(list)

        #add unique contexts
        for pr_id, prof in enumerate(profiles):
            for ctx_id, ctx in enumerate(t_unique):
                node_prof = pr_id
                node_genome = name_node(ctx)
                graph.add_node(node_prof, profile=True, prof=copy(prof))
                graph.add_node(node_genome, profile=False, ctx=copy(ctx))

                score = _profile_similarity(prof, ctx, repeats, same_len=False)
                if score > 0:
                    graph.add_edge(node_prof, node_genome, weight=score, match="unq")
                    profile_updates[pr_id].append(ctx)

        #repetitive ones
        dups = set()
        for ctx_1, ctx_2 in combinations(t_repetitive, 2):
            if ctx_1.equal(ctx_2):
                dups.add(ctx_1)
                dups.add(ctx_2)
        different = [ctx for ctx in t_repetitive if ctx not in dups]

        if different:
            many_rep = different * len(profiles)
            for pr_id, prof in enumerate(profiles):
                for ctx_id, ctx in enumerate(many_rep):
                    node_prof = pr_id
                    node_genome = name_node(ctx)
                    graph.add_node(node_prof, profile=True, prof=copy(prof))
                    graph.add_node(node_genome, profile=False, ctx=copy(ctx))

                    score = _profile_similarity(prof, ctx, repeats, same_len=False)
                    if score >= 0:
                        graph.add_edge(node_prof, node_genome, weight=score,
                                       match="rep")
                        profile_updates[pr_id].append(ctx)

        edges = _max_weight_matching(graph)
        graph.remove_edges_from(deepcopy(graph.edges()))
        graph.add_edges_from(edges)
        # Create new profiles for contexts that couldn't be matched
        for node in nx.isolates(deepcopy(graph)):
            if not graph.node[node]["profile"]:
                profiles.append([graph.node[node]["ctx"]])
        # Update profiles to which new matches were found and retained
        for pr_id in profile_updates:
            for ctx in profile_updates[pr_id]:
                if graph.has_edge(name_node(ctx), pr_id):
                    profiles[pr_id].append(ctx)

    return profiles

def _context_similarity(ctx_ref, ctx_trg, repeats, same_len):
    """
    Compute similarity between two contexts
    """
    def alignment(ref, trg):
        """
        Computes global alignment
        """
        GAP = -2
        def str_abs(s):
            return s if not s.startswith('-') else s[1:]
        def match(a, b):
            mult = 1 if str_abs(a) in repeats or str_abs(b) in repeats else 2
            if a != b:
                return -mult
            else:
                return mult

        l1, l2 = len(ref) + 1, len(trg) + 1
        table = [[0 for _ in range(l2)] for _ in range(l1)]
        if same_len:
            for i in range(l1):
                table[i][0] = i * GAP
            for i in range(l2):
                table[0][i] = i * GAP

        for i, j in product(range(1, l1), range(1, l2)):
            table[i][j] = max(table[i-1][j] + GAP, table[i][j-1] + GAP,
                              table[i-1][j-1] + match(ref[i-1], trg[j-1]))
        return table[-1][-1]

    if len(ctx_trg.left) + len(ctx_trg.right) == 0:
        return 0

    left = alignment(ctx_ref.left, ctx_trg.left)
    right = alignment(ctx_ref.right[::-1], ctx_trg.right[::-1])
    #return float(left + right) / (len(ctx_trg.left) + len(ctx_trg.right))
    return left + right


def _profile_similarity(profile, genome_ctx, repeats, same_len):
    """
    Compute similarity of set of contexts vs one context
    """
    #scores = list(map(lambda c: _context_similarity(c, genome_ctx,
    #                                repeats, same_len), profile))
    scores = [_context_similarity(c, genome_ctx, repeats, same_len) for c in profile]
    return float(sum(scores)) / len(scores)


def _max_weight_matching(graph):
    edges = nx.max_weight_matching(graph, maxcardinality=True)
    unique_edges = set()
    for v1, v2 in edges:
        if not (v2, v1) in unique_edges:
            unique_edges.add((v1, v2))

    # preserve the edge data
    unique_edges = [(v1, v2, graph[v1][v2]) for v1, v2 in unique_edges]
    return unique_edges


def _get_contexts(permutations, repeats):
    """
    Get repeats' contexts
    """
    str_sign = {-1: '-', 1:''}

    WINDOW = 5

    contexts = defaultdict(list)
    for perm in permutations:
        for pos in range(len(perm.blocks)):
            block = perm.blocks[pos]
            if block.block_id not in repeats:
                continue

            left_start = max(0, pos - WINDOW)
            left_end = max(0, pos)
            #left_context = list(map(lambda b: b.signed_id() * block.sign,
            #                        perm.blocks[left_start:left_end]))
            left_context = [f"{str_sign[b.sign * block.sign]}{b.block_id}" for b in
                            perm.blocks[left_start:left_end]]

            right_start = min(len(perm.blocks), pos + 1)
            right_end = min(len(perm.blocks), pos + WINDOW + 1)
            #right_context = list(map(lambda b: b.signed_id() * block.sign,
            #                         perm.blocks[right_start:right_end]))
            right_context = [f"{str_sign[b.sign * block.sign]}{b.block_id}" for b in
                             perm.blocks[right_start:right_end]]

            if block.sign < 0:
                left_context, right_context = (right_context[::-1],
                                               left_context[::-1])

            contexts[block.block_id].append(Context(perm, pos, left_context,
                                                    right_context))
    return contexts
