from collections import defaultdict
import fileinput
import sys

from .io.permutations import parse_auto


def main(permutations_file_list, mapping_file, outfile_handle):
    permutations, write = parse_auto(
        fileinput.input(files=permutations_file_list),
    )

    mapping = load_mapping(
        fileinput.input(mapping_file),
    )

    rename(permutations, mapping)

    write(
        permutations=permutations,
        ofh=outfile_handle,
    )

def load_mapping(mapping_file):
    mapping = {}
    for line in mapping_file:
        if line.startswith('#'):
            continue
        orig_name, new_name = line.strip().split('\t')
        mapping[orig_name] = new_name
    return mapping

def rename(permutations_list, mapping):
    for perm in permutations_list:
        for block in perm.blocks:
            if block.block_id in mapping:
                block.block_id = mapping[block.block_id]
        
