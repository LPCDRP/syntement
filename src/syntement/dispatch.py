import argparse
import os
import sys

from .external.argparse import DefaultSubcommandArgumentParser
from . import (
    __version__,
    cast,
    discern,
    dupreport,
    enum,
    grep,
    match,
    neighbors,
    perm,
    rename,
)


def main():

    head_parser = DefaultSubcommandArgumentParser(
        prog='syntement',
        description='synteny block toolkit',
        epilog = (
            "A Elghraoui, S Mirarab, KM Swenson, F Valafar. "
            "Evaluating impacts of syntenic block detection strategies on rearrangement phylogeny using Mycobacterium tuberculosis isolates. "
            "Bioinformatics, Volume 39, Issue 1, January 2023, btad024. "
            "<https://doi.org/10.1093/bioinformatics/btad024>"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    head_parser.add_argument(
        '-v', '--version',
        action='version',
        help='Print version and exit',
        version=__version__,
    )
    subparsers = head_parser.add_subparsers(
        dest='subparser_name',
    )


    #
    # cast
    #
    cast_parser = subparsers.add_parser(
        'cast',
        help="formulate synteny block elements. (default)",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    head_parser.set_default_subparser('cast')
    cast_parser.add_argument(
        'infiles',
        nargs='+',
        help="alignment or annotation file",
    )
    cast_parser.add_argument(
        '--outdir','-o',
        default='.',
        help="output file directory",
    )
    cast_gff_parser = cast_parser.add_argument_group('GFF options')
    cast_gff_parser.add_argument(
        '--feature-types',
        help="Feature type name(s) to use (CDS, tRNA, gene, etc.; not case sensitive). Pass 'all' to use everything.",
        nargs='+',
        default=['gene'],
    )
    cast_gff_parser.add_argument(
        '--id-tag',
        help=(
            "Annotation qualifier to use as a block identifier (not case sensitive). "
            " This must be consistent across genomes."
            " Features without the qualifier will be skipped."
        ),
        type=str,
        default='gene',
    )
    cast_maf_parser = cast_parser.add_argument_group('MAF options')
    cast_maf_parser.add_argument(
        '--min-block-len',
        type=int,
        default=0,
        help="minimum length of MAF alignments to use",
    )
    cast_parser.set_defaults(func=cast.main)


    #
    # perm
    #
    perm_parser = subparsers.add_parser(
        'perm',
        help="create UniMoG-style permutations file from blocks coords.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    perm_parser.add_argument(
        'coords_file',
        type=argparse.FileType('r'),
        help="blocks coords BED file",
        default='-',
    )
    perm_parser.add_argument(
        '-o', '--outfile',
        type=argparse.FileType('w'),
        help="output permutations file",
        default='-',
    )
    perm_parser.set_defaults(func=perm.main)


    #
    # grep
    #
    grep_parser = subparsers.add_parser(
        'grep',
        help="show blocks that match patterns.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    grep_parser.add_argument(
        'pattern',
        type=str,
        help="block name pattern to search for",
    )
    grep_parser.add_argument(
        'permutations_file',
        type=argparse.FileType('r'),
        help="synteny block permutations file",
    )
    grep_parser.add_argument(
        '-s', '--seqs',
        type=str,
        help="only show matches for these sequence IDs",
        nargs='+',
    )
    grep_parser.add_argument(
        '-C', '--context',
        type=int,
        help="number of synteny blocks of context to show",
        default=5,
    )
    grep_parser.set_defaults(func=grep.main)


    #
    # dupreport
    #
    dupreport_parser = subparsers.add_parser(
        'dupreport',
        help="count and summarize duplicate blocks.",
        description="""
Count and summarize duplicate blocks.
Output columns are:

avg_unique_fraction, n_duplicate_markers, n_duplicate_occurrences, avg_duplicity, max_duplicity, sample_with_max, max_dupe_block_id
""",
    )
    dupreport_parser.add_argument(
        'permutations_file',
        type=str,
        help="synteny block permutations file (default: stdin)",
        nargs='?',
    )
    dupreport_parser.add_argument(
        '-o', '--output-list',
        type=str,
        help="""
Additionally write all duplicated blocks to the file specified here. (default: not written)
The column headers in this file would be: count, block_id, sample
""",
    )
    dupreport_parser.set_defaults(func=dupreport.main)


    #
    # discern -- resolve duplicate blocks
    #
    discern_parser = subparsers.add_parser(
        'discern',
        help="resolve duplicate synteny blocks",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    discern_parser.add_argument(
        'permutations_file',
        type=argparse.FileType('r'),
        help="synteny block permutations or blocks coords BED file",
    )
    discern_parser.add_argument(
        '-t', '--tree-file',
        type=str,
        help="phylogenetic tree file in Newick format",
        required=True,
    )
    discern_parser.add_argument(
        '-j', '--nproc',
        type=int,
        help="number of threads to use",
        default=1,
    )
    discern_parser.add_argument(
        '-o', '--outfile',
        type=argparse.FileType('w'),
        help="output file name for resolved permutations",
        default='-',
    )
    discern_parser.set_defaults(func=discern.main)


    #
    # enum
    #
    enum_parser = subparsers.add_parser(
        'enum',
        help="enumerate synteny blocks",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=
"""
Enumerate each synteny block and output a two-column tab-delimited mapping of the
original name to the assigned number. Useful in conjunction with `syntement rename`
to apply the mapping. For example:

  syntement enum blocks.dat | tee mapping.txt | syntement rename --mapping - blocks.dat > blocks.renamed.dat

This will rename the synteny blocks in blocks.dat according to the enumeration and
store the mapping in mapping.txt.
"""
    )
    enum_parser.add_argument(
        'permutations_file_list',
        metavar='permutations_file',
        type=str,
        help="synteny block permutations file(s) in BED or UniMoG format (default: stdin)",
        nargs='*',
    )
    enum_parser.add_argument(
        '-o', '--outfile',
        help="output mapping filename (default: stdout)",
        type=argparse.FileType('w'),
    )
    enum_parser.set_defaults(func=enum.main)


    #
    # match -- find overlapping blocks from different casts on the same genomes
    #
    match_parser = subparsers.add_parser(
        'match',
        help="match blocks cast from different methods on the same genomes",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    match_parser.add_argument(
        'ref_coords_file',
        help="reference blocks coordinates file (BED or GFF)"
    )
    match_parser.add_argument(
        'qry_coords_file',
        help="query blocks coordinates file (BED or GFF)"
    )
    match_parser.add_argument(
        '-r', '--report',
        help="report file from fracture heatseeker to tag with per-genome overlap information",
    )
    match_parser.set_defaults(func=match.main)

    #
    # neighbors
    #
    neighbors_parser = subparsers.add_parser(
        'neighbors',
        help="create a matrix of extremities and their neighbors in each genome",
    )
    neighbors_parser.add_argument(
        'permutations_file_list',
        metavar='permutations_file',
        type=str,
        help="synteny block permutations file(s) in BED or UniMoG format (default: stdin)",
        nargs='*',
    )
    neighbors_parser.add_argument(
        '-o', '--outfile',
        metavar='OUTFILE',
        help="output tab-separated values file (default: stdout)",
        type=argparse.FileType('w'),
        dest='outfile_handle',
        default='-',
    )
    neighbors_parser.add_argument(
        '--collapse-repeats',
        action='store_true',
        help=(
            'When duplicate blocks have been resolved (as with syntement discern), ignore distinctions between neighbors that are identical but may have been resolved differently.'
        ),
    )
    neighbors_parser.set_defaults(func=neighbors.main)

    #
    # rename
    #
    rename_parser = subparsers.add_parser(
        'rename',
        help="rename synteny blocks according to a mapping file",
    )
    rename_parser.add_argument(
        'permutations_file_list',
        metavar='permutations_file',
        type=str,
        help="synteny block permutations file(s) in BED or UniMoG format (default: stdin)",
        nargs='*',
    )
    rename_parser.add_argument(
        '-m', '--mapping',
        type=str,
        help="two-column, tab-delimited file mapping existing names to new names. Pass '-' to use stdin.",
        dest='mapping_file',
        required=True,
    )
    rename_parser.add_argument(
        '-o', '--outfile',
        metavar='OUTFILE',
        help="output permutations file in same format as the input (default: stdout)",
        type=argparse.FileType('w'),
        dest='outfile_handle',
        default='-',
    )
    rename_parser.set_defaults(func=rename.main)



    args = head_parser.parse_args()

    # pass arguments defined for the specific subparser to its main handler function.
    # If an argument is defined here but not expected by the hander function, we can expect a failure.
    args.func(
        **{
            k:v for k,v in vars(args).items() if k in [
                action.dest for action in subparsers._name_parser_map[args.subparser_name]._actions
            ]
        }
    )
