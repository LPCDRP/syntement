from collections import defaultdict
import fileinput
import itertools
import re

import networkx as nx

from .io.permutations import parse_auto

def main(
    permutations_file_list,
    outfile_handle,
    collapse_repeats,
):
    permutations = parse_auto(
        fileinput.input(files=permutations_file_list),
    )[0]

    adjs_by_genome = survey(permutations)

    extremities = sorted(set(itertools.chain.from_iterable(
        adjs_by_genome.values()
    )))
    genomes = sorted(adjs_by_genome)

    # write header
    outfile_handle.write(
        '\t'.join([''] + genomes) + "\n"
    )

    for ext in extremities:
        row = [ext]
        for genome in genomes:
            if ext not in adjs_by_genome[genome]:
                row.append('DELETED')
            else:
                # TODO: don't assume repeats are resolved.
                #       if ext has multiple copies, length of this list will be > 1
                neighbor = list(adjs_by_genome[genome][ext])[0]
                if collapse_repeats:
                    neighbor = re.sub(r'\.\d+(_[ht])$', r'\1', neighbor)
                row.append(neighbor)
        outfile_handle.write('\t'.join(row) + '\n')


def survey(permutations):
    adj = defaultdict(nx.Graph)

    right_extrem = {
        1 : 'h',
        -1: 't',
    }
    left_extrem = {
        1 : 't',
        -1: 'h',
    }

    for perm in permutations:
        for i in range(len(perm.blocks)):
            this_block = perm.blocks[i]
            # TODO: don't assume circularity
            if i != len(perm.blocks) - 1:
                next_block = perm.blocks[i+1]
            else:
                next_block = perm.blocks[0]

            adj[perm.genome_name].add_edge(
                f"{this_block.block_id}_{right_extrem[this_block.sign]}",
                f"{next_block.block_id}_{left_extrem[next_block.sign]}"
            )

    return adj
