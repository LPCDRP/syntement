import os
import subprocess


def main(permutations_file, output_list):
    extra_args = []
    if output_list:
        extra_args += [
            "-o",
            output_list,
        ]
    if permutations_file:
        extra_args.append(permutations_file)

    subprocess.run(
        [
            f"{os.path.abspath(os.path.dirname(__file__))}/dupreport.bash",
        ] + extra_args,
    )
