# NAME

syntement - format synteny block elements

# SYNOPSIS

**syntement** [*-o outdir*] *alignment.mfa*

**syntement** [*-o outdir*] *blocks_coords.gff* [*additional-sample.gff* [*additional-sample2.gff* ...]]

# DESCRIPTION

**syntement** formulates alignment segments (MAF) or genomic features (GFF) directly, without aggregation, as synteny blocks.

For GFF inputs, the sample names associated with each feature are taken from the first column, and the features are identified by the ID attribute.
It is expected that corresponding features have matching ID fields.

# CITATION

Afif Elghraoui, Siavash Mirarab, Krister M Swenson, Faramarz Valafar.
Evaluating impacts of syntenic block detection strategies on rearrangement phylogeny using Mycobacterium tuberculosis isolates.
Bioinformatics, Volume 39, Issue 1, January 2023, btad024.
https://doi.org/10.1093/bioinformatics/btad024

# SEE ALSO

[maf2synteny](https://github.com/fenderglass/maf2synteny)
