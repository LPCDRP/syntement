# Version 0.2

* Enable syntement to bypass pyinterval dependency by skipping coverage calculation.
* syntement now writes a .genome file to accompany the .bed file, specifying full sequence lengths.
* Genome annotation GFF files can now be cast into permutations and blocks coords.
* Introduced a subcommand structure:
  - `syntement ...` is equivalent to `syntement cast ...`
* New functionality
  - `syntement dupreport` to summarize duplicate block occurrences and counts
  - `syntement discern` to distinguish duplicate blocks based on genomic context.
  - `syntement match` to compare different synteny block casts on the same set of genomes.
  - `syntement perm` to convert blocks coords to UniMog-style permutations.
  - `syntement grep` to find synteny blocks among permutations.
  - `syntement enum` to enumerate synteny blocks.
  - `syntement rename` to rename synteny blocks according to a mapping file.
  - `syntement neighbors` to create a matrix of block extremities vs. their neighbors.
* Breaking change: The `--minBlockLen` option to `syntement cast` is now `--min-block-len`.
* Added `-v`/`--version` option

# Version 0.1.0

Initial release, as described in https://doi.org/10.1093/bioinformatics/btad024
